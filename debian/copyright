Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Files-Excluded: docs/user/rst/images/biohazard.swf
Upstream-Name: docutils

Files: *
Copyright: Public domain
License: public-domain
 The persons who have associated their work with this project (the
 "Dedicator": David Goodger and the many contributors to the Docutils
 project) hereby dedicate the entire copyright, less the exceptions_
 listed below, in the work of authorship known as "Docutils" identified
 below (the "Work") to the public domain.
 .
 The primary repository for the Work is the Internet World Wide Web
 site <https://docutils.sourceforge.io/>.  The Work consists of the
 files within the "docutils" module of the Docutils project Subversion
 repository (http://svn.code.sf.net/p/docutils/code/),
 whose Internet web interface is located at
 <https://sourceforge.net/p/docutils/code>.  Files dedicated to the
 public domain may be identified by the inclusion, near the beginning
 of each file, of a declaration of the form::
 .
     Copyright: This document/module/DTD/stylesheet/file/etc. has been
                placed in the public domain.
 .
 Dedicator makes this dedication for the benefit of the public at large
 and to the detriment of Dedicator's heirs and successors.  Dedicator
 intends this dedication to be an overt act of relinquishment in
 perpetuity of all present and future rights under copyright law,
 whether vested or contingent, in the Work.  Dedicator understands that
 such relinquishment of all rights includes the relinquishment of all
 rights to enforce (by lawsuit or otherwise) those copyrights in the
 Work.
 .
 Dedicator recognizes that, once placed in the public domain, the Work
 may be freely reproduced, distributed, transmitted, used, modified,
 built upon, or otherwise exploited by anyone for any purpose,
 commercial or non-commercial, and in any way, including by methods
 that have not yet been invented or conceived.

Files: docutils/__main__.py
       docutils/parsers/commonmark_wrapper.py
       docutils/parsers/recommonmark_wrapper.py
       docutils/utils/error_reporting.py
       docutils/utils/math/__init__.py
       docutils/utils/math/mathalphabet2unichar.py
       docutils/utils/math/mathml_elements.py
       docutils/utils/math/tex2mathml_extern.py
       docutils/utils/punctuation_chars.py
       docutils/writers/html5_polyglot/italic-field-names.css
       docutils/writers/html5_polyglot/minimal.css
       docutils/writers/html5_polyglot/plain.css
       docutils/writers/html5_polyglot/responsive.css
       docutils/writers/latex2e/docutils.sty
       docutils/writers/xetex/__init__.py
       test/test_CLI.py
       test/test_parsers/test_recommonmark/test_block_quotes.py
       test/test_parsers/test_recommonmark/test_bullet_lists.py
       test/test_parsers/test_recommonmark/test_enumerated_lists.py
       test/test_parsers/test_recommonmark/test_html_blocks.py
       test/test_parsers/test_recommonmark/test_inline_markup.py
       test/test_parsers/test_recommonmark/test_line_length_limit_default.py
       test/test_parsers/test_recommonmark/test_line_length_limit.py
       test/test_parsers/test_recommonmark/test_literal_blocks.py
       test/test_parsers/test_recommonmark/test_misc.py
       test/test_parsers/test_recommonmark/test_section_headers.py
       test/test_parsers/test_recommonmark/test_targets.py
       test/test_parsers/test_rst/test_directives/test__init__.py
       test/test_parsers/test_rst/test_directives/test_code_parsing.py
       test/test_parsers/test_rst/test_line_length_limit_default.py
       test/test_parsers/test_rst/test_line_length_limit.py
       test/test_parsers/test_rst/test_root_prefix.py
       test/test_parsers/test_rst/test_source_line.py
       test/test_transforms/test_filter_messages.py
       test/test_transforms/test_smartquotes.py
       test/test_utils/test_math/test__init__.py
       test/test_utils/test_math/test_mathml_elements.py
       test/test_utils/test_math/test_tex2mathml_extern.py
       test/test_writers/test_latex2e_misc.py
       tools/dev/generate_punctuation_chars.py
       tools/docutils-cli.py
       tools/rst2html5.py
Copyright: 2010-2024 Günter Milde
License: BSD-2-clause

Files: docutils/writers/_html_base.py
       docutils/writers/html5_polyglot/__init__.py
Copyright: 2016 David Goodger
           2005-2016 Günter Milde
License: BSD-2-clause

Files: docs/user/smartquotes.txt
       docutils/utils/smartquotes.py
Copyright: 2003 John Gruber
           2004, 2007 Chad Miller
           2010-2023 Günter Milde
License: BSD-3-clause and BSD-2-clause

Files: docutils/utils/math/latex2mathml.py
Copyright: 2005 Jens Jørgen Mortensen
           2010, 2021, 2024 Günter Milde
License: BSD-2-clause

Files: docutils/utils/math/math2html.py
       docutils/writers/html5_polyglot/math.css
Copyright: 2009-2011 Alex Fernández
           2021 Günter Milde
License: BSD-2-clause

Files: docs/dev/enthought-plan.txt
       docs/dev/enthought-rfp.txt
Copyright: 2004 Enthought, Inc.
License: BSD-3-clause

Files: docutils/utils/roman.py
Copyright: 2001 Mark Pilgrim and Contributors
License: Zope-2.1

Files: tools/editors/emacs/rst.el
       tools/editors/emacs/tests/ert-buffer.el
Copyright: 2003-2017 Free Software Foundation, Inc.
License: GPL-3+

Files: docutils/writers/html5_polyglot/tuftig.css
Copyright: 2014 Dave Liepmann
           2020 Günter Milde
License: Expat and BSD-2-clause

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems the text of the GNU General Public License
 version 3 can be found in /usr/share/common-licenses/GPL-3.

License: BSD-2-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 * Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
 .
 * Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
 .
 * Neither the name of the author nor the names of its contributors
   may be used to endorse or promote products derived from this
   software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: Zope-2.1
 Zope Public License (ZPL) Version 2.1
 .
 A copyright notice accompanies this license document that identifies the
 copyright holders.
 .
 This license has been certified as open source. It has also been designated as
 GPL compatible by the Free Software Foundation (FSF).
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions in source code must retain the accompanying copyright
 notice, this list of conditions, and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the accompanying copyright
 notice, this list of conditions, and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 .
 3. Names of the copyright holders must not be used to endorse or promote
 products derived from this software without prior written permission from the
 copyright holders.
 .
 4. The right to distribute this software or to use it for any purpose does not
 give you the right to use Servicemarks (sm) or Trademarks (tm) of the
 copyright
 holders. Use of them is covered by separate agreement with the copyright
 holders.
 .
 5. If any files are modified, you must cause the modified files to carry
 prominent notices stating that you changed the files and the date of any
 change.
 .
 Disclaimer
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY EXPRESSED
 OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO
 EVENT SHALL THE COPYRIGHT HOLDERS BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
